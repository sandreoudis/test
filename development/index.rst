Κατασκευή
=========

Εδώ θα περιγράφεται λεπτομερώς η διαδικασία της κατασκευής. 
Στοιχεία που χρειάζονται:

* ευκολία κατασκευής (εύκολο, μέτριο, δύσκολο)
* χρόνος κατασκευής (ίσως σε ώρες εργασίας)
* περιγραφή της κατασκευής (με λεπτομέρειες)
* φωτογραφίες κατά την κατασκευή (ειδικά σε ευαίσθητα/δύσκολα σημεία)
* σχεδιαγράμματα (τύπου ΙΚΕΑ ίσως;;;; )

