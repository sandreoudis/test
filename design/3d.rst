Απεικόνιση 3D
=============

Εδώ θα περιγράφεται η απεικόνιση του εργαλείου με σχέδια σε 3d και φωτογραφίες.

Ισομετρική Απεικόνιση
---------------------

.. figure:: img/1.jpg
    :width: 400px
    :align: center
    :height: 200px
..    :alt: alternate text
    :figclass: align-center
    
Ισομετρική απεικόνιση. (Το σχέδιο έχει εκδοθεί υπό την άδεια π.χ. CC-BY-SA και ανήκει στο `farmhack.org <https://farmhack.org/tools/root-washer>`_. Εδώ χρησιμοποιείται μόνο ως πρότυπο).
       
Εμπρόσθια Απεικόνιση
--------------------
 
.. figure:: img/2.jpg
    :width: 400px
    :align: center
    :height: 200px
..    :alt: alternate text
    :figclass: align-center

Εμπρόσθια απεικόνιση. (Το σχέδιο έχει εκδοθεί υπό την άδεια π.χ. CC-BY-SA και ανήκει στο `farmhack.org <https://farmhack.org/tools/root-washer>`_. Εδώ χρησιμοποιείται μόνο ως πρότυπο).

Πλαϊνή Απεικόνιση
-----------------

.. figure:: img/3.jpg
    :width: 400px
    :align: center
    :height: 200px
..    :alt: alternate text
    :figclass: align-center
    
Πλαϊνή απεικόνιση. (Το σχέδιο έχει εκδοθεί υπό την άδεια π.χ. CC-BY-SA και ανήκει στο `farmhack.org <https://farmhack.org/tools/root-washer>`_. Εδώ χρησιμοποιείται μόνο ως πρότυπο).
    
Συνέχεια του κειμένου ...
    
    